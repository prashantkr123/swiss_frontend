import * as actionTypes from './actions.js';
import axios from 'axios';
export const addPlayer = (player_name, tournament_id) => {
  let data = {
      player:player_name,
      tournament_id: tournament_id
    }
    return dispatch => {
        axios.post('api/players/', data). then( response => {
              console.log(response.data)
               dispatch(appendPlayer(response.data));
            })
            .catch(error => {
        if(error.response.status === 409 )
           alert('CANNOT ADD AS PLAYER IS ALREADY ASSOCIATED WITH THE TOURNAMENT')
        else if(error.response.status === 400) {
          alert('CANNOT ADD PALYER AS TOURNAMENT HAS BEEN FINISHED')
        }
        else if(error.response.status === 401) {
          alert('CANNOT ADD PALYER AS TOURNAMENT IS IN PROGRESS')
        }
    })
    };


}

export const getSwissPairs = () => {
    return {
       type: actionTypes.GET_SWISS_PAIRS
       
    

    }
 


}
export const reportMatch = () => {
    return {
       type: actionTypes.REPORT_MATCH
     

    } 


}
export const appendPlayer = (player) => {
   return {

      type: actionTypes.ADD_PLAYER,
      player: player


   }

}
export const setPlayers = (players) => {
    return  {

     type: actionTypes.SET_PLAYERS,
     players: players
 
    };

}; 
export const initPlayers = (tournament_id) => {
    return dispatch => {
        axios.get('api/players/', { params: { 'tournament_id': tournament_id } 
   }). then( response => {
          console.log(response.data)
               dispatch(setPlayers(response.data));
            })
            .catch( error => {
            } );
    };
};