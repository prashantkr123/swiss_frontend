export const ADD_PLAYER = 'ADD_PLAYER';
export const GET_SWISS_PAIRS = 'GET_SWISS_PAIRS';
export const REPORT_MATCH = 'REPORT_MATCH';
export const INIT_PLAYERS = 'INIT_PLAYERS';
export const SET_PLAYERS = 'SET_PLAYERS';