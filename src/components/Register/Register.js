import React, { Component } from 'react';
import axios from 'axios';
import './Register.css';


class Register extends Component {
    state = {
        username: '',
        password: '',
        first_name: '',
        last_name: '',
        country_code: ''
        
    }
    
   loginHandler = () => {

  this.props.history.push('/login')



}    
    
   registerHandler = () =>  {
    const data = {
        username: this.state.username,
        password: this.state.password,
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        country_code: this.state.country_code

    }
    axios.post('/api/signup/', data)
    .then(response => {
      console.log('sending the signup request')
      this.props.history.push('/login');

    }).catch(error => {
      if(error.response.status === 409 )
         alert('USERNAME IS ALREADY REGISTERED')
      else if(error.response.status === 400)
           alert('PASSWORD SHOULD BE MINIMUM OF 8 CHARACTERS')
    })

   }
   
    render () {
      
            
        return (
        <div>
        
  <div class="row">
   <div class="col-md-6 col-sm-6 col-xs-12">
     <div class="form-group ">
      <label class="control-label requiredField" for="name">
       Username
       
      </label>
      <input class="form-control" value={this.state.username}
           onChange={(event) => this.setState({username: event.target.value})} 
            id="name" name="name" placeholder="username" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="text">
       Password
       
      </label>
      <input class="form-control" id="text" value={this.state.password}
            onChange={(event) => this.setState({password: event.target.value})}
             name="text" placeholder="password " type="password"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="name1">
       First Name
       
      </label>
      <input class="form-control" id="name1" value={this.state.first_name}
            onChange={(event) => this.setState({first_name: event.target.value})}
              name="name1" placeholder="first name" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="name2">
       Last Name
       
      </label>
      <input class="form-control" id="name2" value={this.state.last_name} 
           onChange={(event) => this.setState({last_name: event.target.value})}
           name="name2" placeholder="last name" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="select">
       Country of Origin
       
      </label>
      <select class="select form-control" value={this.state.country_code}
              onChange={(event) => this.setState({country_code: event.target.value})}
                id="select" name="select">
       <option value="USA">
        USA
       </option>
       <option value="INDIA">
          INDIA
       </option>
       <option value="UK">
        UK
       </option>
      </select>
     </div>
     <div class="form-group">
      <div>
       <button class="btn btn-primary btn-lg" name="submit" type="submit"
           onClick={this.registerHandler} > 
        REGISTER
       </button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Already Registered?   &nbsp; &nbsp; 
       <button class="btn btn-primary btn-lg" name="submit" type="submit"
              onClick={this.loginHandler} >
        LOGIN
       </button>
      </div>
     </div>
   </div>
  </div>
 </div>
        )

    }
}

export default Register; 