import React, { Component } from 'react';
import axios from 'axios';
import {Modal, Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';
import {initPlayers} from '../../store/actionCreator';


class Match extends Component {

constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this);
  }


   state = {
       tournament_id: null,
       player_standings: null,
       rounds: null,
       isModalOpen: false,
       current_round: null,
       swiss_list: null,
       winner_list: [],
       selectedValue: null       
   }


openModal() {
      this.setState({ isModalOpen: true })
    }

    closeModal() {
      this.setState({ isModalOpen: false })
    }




getSwissPairs(round) {
   if (this.state.current_round === (round - 1)) {

  this.setState({ isModalOpen: true });
    axios.get('api/swiss_pairings/', {params: {'tournament_id': this.state.tournament_id,
                                                'round_num': this.state.current_round
                                              }
                                        
                                    }
                              ).then(response => {
                  this.setState({swiss_list: response.data});
                                              }).catch(error => {})
            }
      else if(this.state.current_round < (round - 1))
      {
          alert('HOLD ON: PREVIOUS ROUNDS ARE PENDING')
      }
      else {
         alert('HEY! THIS ROUND SEEMS FINISHED')
      }

}




handleChange(event){
    this.state.winner_list.push(event.target.value)
  }



reportMatch() {

  const data = {
        'player_one': [],
        'player_two': [],
        'winner': this.state.winner_list 
    }
    
    if(this.state.swiss_list){
      this.state.swiss_list.map(tour4=>{
        return (
                   data.player_one.push(tour4[1]),
                      
                   data.player_two.push(tour4[3])
              );
      })
    }
  
    axios.post('api/tournament/'+this.state.tournament_id+'/status/'+this.state.current_round+'/data/', data)
    .then(response => {
         
     axios.get('api/standings/', { params: { 'tournament_id': this.state.tournament_id } 
   }).then(response => {
    this.setState({player_standings:response.data.player_stats})
    this.setState({current_round: response.data.current_round})
   })
   

    }).catch(error => {
        
    }) 

  this.setState({isModalOpen: false})
  this.setState({winner_list: []})  
}





 componentDidMount () {
    let a = this.props.location.pathname;
    console.log(a);
    let arr = a.split('/'); 
    let final = arr[arr.length-2];
    this.setState({tournament_id: final});
    this.props.onInitPlayers(final);
   


   axios.get('api/standings/', { params: { 'tournament_id': final } 
   }).then(response => {
    this.setState({player_standings:response.data.player_stats});
    this.setState({rounds: response.data.rounds});
    this.setState({current_round: response.data.current_round});
   }).catch(error => {
   })


  }
  





   render () {





     let tour= '';
      if(this.props.player_list){
      tour = this.props.player_list.map(tour=>{
        return (<div key={tour.id}>{tour.name} </div>);
      })
    }




    let tour11= '';
    let tour12='';
    let tour13='';
    let tour14='';
      if(this.state.player_standings){
      tour11 = this.state.player_standings.map(tour11=>{
        return (<tr> <td>{tour11.player_id} </td> 
                      </tr>);
      })
    tour12 = this.state.player_standings.map(tour12=>{
        return (<tr>  
                      <td>{tour12.player_name} </td> 
                      </tr>);  
      })
    tour13 = this.state.player_standings.map(tour13=>{
        return (<tr>  
                      <td>{tour13.wins} </td> 
                       </tr>);
    })
    tour14 = this.state.player_standings.map(tour14=>{
        return (<tr>  
                       <td>{tour14.lost}</td></tr>);
      })
  }
   
let tour3 = '';
if(this.state.rounds) {
   tour3 = this.state.rounds.map(tour3 =>{
  
    return (
      <tr><br /><Button bsStyle="primary"
          bsSize="medium" 
          onClick = {() => this.getSwissPairs(tour3) }
          > <p>Round &nbsp; <b>{tour3}</b></p> 
          </Button><br/></tr>
          )
})
}



let tour4= '';
      if(this.state.swiss_list){
      tour4 = this.state.swiss_list.map(tour4=>{
        return (<tr>  
                      <td>{tour4[1]} </td>  &nbsp;&nbsp;&nbsp;&nbsp; 
                       <td>{tour4[3]}</td>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <td>
                       
                       <select value={this.state.selectedValue} 
                                   onChange={this.handleChange}
                                   id = "dropdown" 
                                   class="form-control" >
                              <option value="Select Winner" > Select Winner </option>
                              <option value={tour4[1]} > {tour4[1]} </option>
                              <option value={tour4[3]} > {tour4[3]} </option>
                            </select>
                            
                       </td>
               </tr>);
      })
    }




      
       return (

           <div>
             <div class="row">
             <div class="col-md-6">
                <div class="list-group ">
                    <a class="list-group-item active">PLAYER LIST</a> 
                    <a class="list-group-item ">{tour}</a> 
                </div>
            </div>
          <div class="col-md-6 ">

                <div class="list-group ">
                    <a  class="list-group-item active">PLAYER STANDINGS</a> 
                    <table class = "table">
                    <thead>
      <tr>
         <th>Player ID</th>
         <th>Player Name</th>
         <th>Wins</th>
         <th>Losses</th>
      </tr>
   </thead>
             <tbody>
      <tr>
         <td>{tour11}</td>
         <td>{tour12}</td>
         <td>{tour13}</td>
         <td>{tour14}</td>
         
      </tr>
    </tbody>
    

   </table>
                    
                </div>
            </div>
           </div>
           <div class="text-uppercase">
                <a class="list-group-item active">ROUND INFO</a>
                  <a class="list-group-item ">
              {tour3} 
                </a>
                
            </div>
        <div>
            <Modal show={this.state.isModalOpen} onHide={this.close}>
          <Modal.Header >
            <Modal.Title>REPORT MATCH </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {tour4}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" bsSize="medium" 
                  onClick={() => this.reportMatch() }  >Submit</Button>
            <Button bsStyle="primary" bsSize="medium" 
                       onClick={() => this.closeModal()} >Close</Button>
          </Modal.Footer>
        </Modal>
      


        </div>

        </div>
       )
   }
}

const mapStateToProps = state => {
    return {
        player_list: state.player_list
    };
}
const mapDispatchToProps = dispatch => {
  
    return {
        onInitPlayers: (tournament_id) => dispatch(initPlayers(tournament_id))
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(Match);




