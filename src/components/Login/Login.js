import React, { Component } from 'react'; 
import axios from 'axios';
import './Login.css'

class Login extends Component {
    state = {
        username: '',
        password: '',
        token: null
        
    }
    
      
    
   postHandler = () => {
    const data = {
        username: this.state.username,
        password: this.state.password
    };
    axios.post('/api/login/', data)
    .then(response => {
      window.localStorage.setItem('X-Auth-Token', response.data.token);
      this.setState({token: response.data.token});
      this.props.history.push('/tournaments')
      

    }).catch(error => {
        console.log(error);
    });

   }
   checkToken = () => {
        const token = window.localStorage.getItem('X-Auth-Token');
        console.log(token);
   }
    render () {
      
            
        return (






 <div class="container">
  
  <form col-md-4 mx auto>
    <div class="form-group ">
      <label for="usr">USERNAME</label>
      <input type="text" class="form-control" value={this.state.username}
        onChange={(event) => this.setState({username: event.target.value})} id="usr" />
      </div>
     
    <div class="form-group">
      <label for="pwd">PASSWORD</label>
      <input type="password" class="form-control" value={this.state.password} 
      onChange={(event) => this.setState({password: event.target.value})} id="pwd" />
    </div>
    <div >
      <center> <button type="button" class="btn btn-primary btn-sm btn-block" 
                 onClick={this.postHandler}><b>LOGIN</b></button>
      </center>

    </div>
  </form>
</div>
  
        );

    }
}

export default Login; 