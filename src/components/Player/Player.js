import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';
import * as help from '../../store/reducer';
import {initPlayers, addPlayer} from '../../store/actionCreator';
class Player extends Component {
   state = {
       player_name:null,
       tournament_id: null
       
   }
   
 
  componentDidMount () {
    let a = this.props.location.pathname;
    var final = a.substr(a.lastIndexOf('/') + 1);
    console.log(final)
    this.setState({tournament_id: final});
    this.props.onInitPlayers(final);
  }



  addPlayer=(event)=>{
    this.setState({player_name:event.target.value})
  }



tournamentStartHandler = () => {
  console.log(this.state.tournament_id)
  let a = this.state.tournament_id;
  axios.get('api/matchvalid/'+a).then(response => {
    this.props.history.push('/tournaments/'+a+'/start'); 
       
   }).catch(error => {
    if(error.response.status === 400 )
       alert('MAKE SURE THAT NUMBER OF PLAYERS IS POWER OF 2 AND IS MUST BE >= 2')
    else if(error.response.status === 401)
      alert('HEY TOURNAMENT HAS BEEN FINISHED')
   })

}




   render () {
    
      let tour11 = '';
      let tour12 = '';
      if(this.props.player_list) {
      tour11 = this.props.player_list.map(tour11=>{
        return (<div >{tour11.id}</div>)
      })
      tour12 = this.props.player_list.map(tour12=>{
        return (<div >{tour12.name}</div>)
      })
    }
       return (

           <div>
         <div class="row">
  <div class="col-md-6">
    <input type="text" class="form-control input-lg" placeholder="player name" onChange={this.addPlayer} />
  </div>
  <div class="col-md-3">

   <button  type="button" class="btn btn-primary btn-lg" onClick={
       () => this.props.onAddPlayer(this.state.player_name, this.state.tournament_id)
                              }>ADD PLAYER</button>

  </div>
  <div class="col-md-3">

   <button  type="button" class="btn btn-primary btn-lg" onClick={this.tournamentStartHandler}>
       START TOURNAMENT</button>

  </div>
  </div>
  <br />

  <table class="table">
    <thead>
      <tr>
        <th>Player ID</th>
        <th>Player Name</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td> {tour11} </td>
        <td> {tour12} </td>
      </tr>
    </tbody>
    </table>
               
           </div>
       )
   }
}

const mapStateToProps = state => {
    return {
        player_list: state.player_list
    };
}
const mapDispatchToProps = dispatch => {
  
    return {
        onAddPlayer: (player_name, tournament_id) => dispatch(addPlayer(player_name, tournament_id)),
        onInitPlayers: (tournament_id) => dispatch(initPlayers(tournament_id))
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(Player);