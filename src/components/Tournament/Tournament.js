import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './Tournament.css';




class Tournament extends Component {
   state = {
       tournament_list: null,
       tournament_name:null
       
   }
   
   
  componentDidMount () {
axios.defaults.headers.common['X-Auth-Token']=window.localStorage.getItem('X-Auth-Token')
   axios.get('api/tournaments/')
   .then(response => {
    console.log(response.data)
    this.setState({tournament_list:response.data})
       
   }).catch(error => {
   })
  }

  addTournament=(event)=>{
    this.setState({tournament_name:event.target.value})
  }





  addTournamentHandler = ()=>{
    let data = {
      tournament:this.state.tournament_name
    }
    axios.post('api/tournaments/',data)
    .then(response=>{
      let saman = [...this.state.tournament_list]
      saman.push(response.data)
      this.setState({tournament_list:saman})
    }).catch(error => {
      alert('TOURNAMENT ALREADY EXISTS')
       
   })
  }





   render () {
    
      let tour11 = ''
      let tour12 = ''
      if(this.state.tournament_list){
      tour11 = this.state.tournament_list.map(tour11=>{
        return (<div key={tour11.id}><Link to={`/tournaments/${tour11.id}`} 
                            >{tour11.name}</Link></div>);
      })
      tour12 = this.state.tournament_list.map(tour12=>{
        return (<div key={tour12.id}>

                  {(() => {
              switch (tour12.status) {
                case 1:   return "NOT STARTED";
                case 2: return "RUNNING";
                case 3:  return "FINISHED";
                default:      return "erre";
              }
            })()}

          </div>)
    })

}

       return (
     <div>        
       <form>
    <div class="form-group">
      <label for="inputdefault"></label>
      <input class="form-control" 
         placeholder="tournament name" id="textbox" type="text" onChange={this.addTournament} /> 
         <br />
         <center> <button type="button" class="btn btn-info btn-sm btn-block" 
                 onClick={this.addTournamentHandler}><b>CREATE TOURNAMENT</b></button>
      </center>
             
        </div>
        </form>
        <table class="table">
    <thead>
      <tr>
        <th>Tournament Name</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td> {tour11} </td>
        <td>  {tour12} </td>
      </tr>
    </tbody>
    </table>
    
               
           </div>
        
       );
   }
}

export default Tournament;