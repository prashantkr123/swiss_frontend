import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import Login from './components/Login/Login';
import Tournament from './components/Tournament/Tournament';
import Player from './components/Player/Player';
import Match from './components/Match/Match';
import Register from './components/Register/Register';




// axios.defaults.baseURL="http://localhost:8000";
 axios.defaults.baseURL = `http://${window.location.host}`
axios.defaults.headers.common['X-Auth-Token']=window.localStorage.getItem('X-Auth-Token');

 class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          
        </header>
        <div>
          <BrowserRouter>
            <Switch>
              
              <Route path="/register" exact component={Register} />
              <Route path="/login" exact component={Login} />
              <Route path="/tournaments/" exact component={Tournament} />
              <Route path="/tournaments/:tournament_id" exact component={Player} />
              <Route path="/tournaments/:tournament_id/start" exact component={Match} />
              <Redirect from="/" to='/register' />
            </Switch>
          </BrowserRouter>
          </div>
      </div>
    );
  }
} 



export default App;
