import React, {Component} from 'react';
import  Login from '../components/Login/Login';
import Tournament from '../components/Tournament/Tournament';
import {Route, NavLink} from 'react-router-dom';
class Swiss extends Component {

  render () {

    return (
      <div>
      <Login />
      <Route path="/tournaments" exact component={Tournament}/> 
      </div>
      )
  }
}

export default Swiss; 