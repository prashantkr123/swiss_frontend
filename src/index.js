import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import axios from 'axios';
import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducer from './store/reducer';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(
       applyMiddleware(thunk)
  ));


// axios.defaults.baseURL="http://localhost:8000";
 axios.defaults.baseURL = `http://${window.location.host}`



 const app = (
   <Provider store={store}>
    <BrowserRouter>
        <App />
    </BrowserRouter>
    </Provider>
  );

ReactDOM.render(app, document.getElementById('root'))
registerServiceWorker();